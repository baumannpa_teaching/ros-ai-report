# Report Template
This repository contains an english report template for the course "KI in der Robotik" of the HTW Berlin.

__On this page__
* [General](#general)
* [Requirements](#requirements)
* [Compilation](#compilation)

## General
The content is orientated on scientific writing for a B.SC. degree (see links provided below for further information). As for only being a report, its content may differ from the thesis setup.

__Document conception is based on:__
* Bachelor Thesis: [https://ai-bachelor.htw-berlin.de/files/Stg/AI/richtlinie_ba-arbeit_ai_06_02_13.pdf](https://ai-bachelor.htw-berlin.de/files/Stg/AI/richtlinie_ba-arbeit_ai_06_02_13.pdf)
* Scientific writing in general: [http://christianherta.de/lehre/HTW/richtlinie-wissenschaftliche-Arbeiten.pdf](http://christianherta.de/lehre/HTW/richtlinie-wissenschaftliche-Arbeiten.pdf)


## Requirements
To compile the template the following software is required:
* lualatex (TeX)
* biber (citation)

## Compilation
The templates have been tested on Linux (Ubuntu), Windows and macOS with the latest version of the software and should be compatible with any IDE. For instructions how to get lualatex with biber installed and running, please refer to your operating system's manual/online help.

__Generate / Update PDF__  

In a basic shell, you have to perform three steps to generate the output. First compile the main.tex

```
lualatex report.tex
```

Afterwards generate the references

```
biber report.bcf
```

As the last step, the tex file has to be recompiled for linking to the references

```
lualatex report.tex
```
